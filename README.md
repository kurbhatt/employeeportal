## Employee Portal

- Portal developed using Java 8, Spring Boot, Swagger
- MySQL table creation file added in /resources directory
- Once application starts, it will be available on http://localhost:8081/swagger-ui.html
