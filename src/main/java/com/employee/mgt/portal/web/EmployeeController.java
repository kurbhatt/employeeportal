package com.employee.mgt.portal.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employee.mgt.portal.data.jpa.Employee;
import com.employee.mgt.portal.data.jpa.dto.GenericResponse;
import com.employee.mgt.portal.service.EmployeeService;

@RestController
@RequestMapping(value = "/api/emp/")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<GenericResponse<Employee>> findEmployee(
      @RequestParam(value = "id", required = false) Long id) {
        return ResponseEntity.ok(this.employeeService.findEmployee(id));
    }

    @PutMapping
    public ResponseEntity<GenericResponse<Employee>> upsertEmployee(
      @RequestBody Employee employee) {
        return ResponseEntity.ok(this.employeeService.saveEmployee(employee));
    }

    @DeleteMapping
    public ResponseEntity<GenericResponse<Employee>> deleteEmployee(
      @RequestParam(value = "id", required = false) Long id) {
        return ResponseEntity.ok(this.employeeService.deleteEmployee(id));
    }
}
