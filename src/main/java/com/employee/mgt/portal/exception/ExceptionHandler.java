package com.employee.mgt.portal.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.employee.mgt.portal.data.jpa.dto.GenericResponse;

@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleInternal(final RuntimeException ex, final WebRequest request) {
        // print error
        ex.printStackTrace();
        final GenericResponse bodyOfResponse = new GenericResponse(
          "Please try again later", "InternalError");
        return new ResponseEntity<>(bodyOfResponse, new HttpHeaders(),
                                          HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
