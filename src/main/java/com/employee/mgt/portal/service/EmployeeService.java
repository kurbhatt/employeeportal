package com.employee.mgt.portal.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.mgt.portal.data.jpa.Employee;
import com.employee.mgt.portal.data.jpa.dto.GenericResponse;
import com.employee.mgt.portal.data.jpa.repository.EmployeeRepository;
import com.employee.mgt.portal.util.Utility;

@Service
public class EmployeeService {

    @Autowired
    private Utility utility;
    @Autowired
    private EmployeeRepository employeeRepository;

    public GenericResponse<Employee> findEmployee(Long id) {
        GenericResponse<Employee> response = new GenericResponse<>();
        if (id == null) {
            // find all
            Iterable<Employee> allEmployees = this.employeeRepository.findAll();
            response.setAllData(this.utility.makeCollection(allEmployees));
        } else {
            // find by id
            Optional<Employee> employee = this.employeeRepository.findById(id);
            if (!employee.isPresent()) {
                response.setStatus(0);
                response.setError("Employee not found with given id");
                return response;
            }
            response.setData(employee.get());
        }
        response.setStatus(1);
        response.setMessage("Success");
        return response;
    }

    public GenericResponse<Employee> saveEmployee(Employee employee) {
        GenericResponse<Employee> response = new GenericResponse<>();
        if (employee.getId() == null) {
            // save
            this.employeeRepository.save(employee);
        } else {
            // update
            Optional<Employee> existing = this.employeeRepository.findById(employee.getId());
            if (!existing.isPresent()) {
                response.setStatus(0);
                response.setError("Employee not found with given id");
                return response;
            }
            Employee existingEmployee = existing.get();
            this.copyData(employee, existingEmployee);
            this.employeeRepository.save(existingEmployee);
        }
        response.setStatus(1);
        response.setMessage("Success");
        return response;
    }

    public GenericResponse<Employee> deleteEmployee(Long id) {
        GenericResponse<Employee> response = new GenericResponse<>();
        if (id == null) {
            response.setStatus(0);
            response.setError("Invalid id");
            return response;
        } else {
            // find by id and delete
            Optional<Employee> employee = this.employeeRepository.findById(id);
            if (!employee.isPresent()) {
                response.setStatus(0);
                response.setError("Employee not found with given id");
                return response;
            }
            this.employeeRepository.delete(employee.get());
        }
        response.setStatus(1);
        response.setMessage("Success");
        return response;
    }

    private void copyData(Employee source, Employee destination) {
        destination.setName(source.getName());
        destination.setDesignation(source.getDesignation());
        destination.setSalary(source.getSalary());
    }
}
