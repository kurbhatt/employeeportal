package com.employee.mgt.portal.data.jpa.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.employee.mgt.portal.data.jpa.Employee;

public interface EmployeeRepository  extends PagingAndSortingRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {

}
